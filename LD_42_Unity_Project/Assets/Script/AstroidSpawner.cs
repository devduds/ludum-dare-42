﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AstroidSpawner : MonoBehaviour {
	[SerializeField]
	private GameObject[] AsteroidPrefabs;
	[SerializeField]
	private GameObject[] SetSizePrefabs;
	[SerializeField]
	private GameObject Snek;
	[SerializeField]
	private GameObject hole;

	public GameObject player;
	public int Difficulty;
	private float startTime;
	private float currentTime;
	private float totalDistanceTraveled;
	private float timeTotal;
	private Vector3 lastPosition;
	// Use this for initialization
	void Start () {
		
		//SpawnAsteroids(NumAstroidsOnScreen);
		startTime = Time.time;
	}
	
	// Update is called once per frame
	void Update () {
		timeTotal += Time.deltaTime;
		if (timeTotal>3f) {
			timeTotal = 0f;
			SpawnObject (randomSpaceObject(), Random.Range(20,35), .1f);
			SpawnObject (randomSpaceObject(), Random.Range(5,15), .12f);
			SpawnObject (randomSpaceObject(), Random.Range(-15,0), .13f);
			SpawnObject (randomSpaceObject(), Random.Range(-35,-20), .14f);
		}

		if ((player.transform.position.x-lastPosition.x) >= 25f) {
			totalDistanceTraveled += (player.transform.position.x-lastPosition.x);
			lastPosition = player.transform.position;
			CreateRow ();
			currentTime = 0;
			setDifficulty ();
		}
	}

	private void setDifficulty(){
		if (totalDistanceTraveled/4 < 10) {
			Difficulty = 1;
		}
		else if (totalDistanceTraveled/4 < 20) {
			Difficulty = 2;
		}
		else if (totalDistanceTraveled/4 < 30) {
			Difficulty = 3;
		}
		else if (totalDistanceTraveled/4 < 40) {
			Difficulty = 4;
		}
		else if (totalDistanceTraveled/4 < 50) {
			Difficulty = 5;
		}
		else if (totalDistanceTraveled/4 < 60) {
			Difficulty = 6;
		}
		else if (totalDistanceTraveled/4 < 70) {
			Difficulty = 7;
		}
		else if (totalDistanceTraveled/4 < 80) {
			Difficulty = 8;
		}
		else if (totalDistanceTraveled/4 < 90) {
			Difficulty = 9;
		}
		else if (totalDistanceTraveled/4 < 100) {
			Difficulty = 10;
		}
		else if (totalDistanceTraveled/4 < 125) {
			Difficulty = 12;
		}
		else if (totalDistanceTraveled/4 < 150) {
			Difficulty = 15;
		}else if (totalDistanceTraveled/4 < 200) {
			Difficulty = 20;
		}else if (totalDistanceTraveled/4 < 300) {
			Difficulty = 21;
		}
	}


	private void CreateRow(){
		switch (Random.Range (0, Difficulty)) {
		case 0:
			SpawnObject (randomSpaceObject(), transform.position.y, .21f);
			SpawnObject (randomSpaceObject(), Random.Range(20,35), .12f);
			SpawnObject (randomSpaceObject(), Random.Range(5,15), .13f);
			SpawnObject (randomSpaceObject(), Random.Range(-15,0), .12f);
			SpawnObject (randomSpaceObject(), Random.Range(-35,-20), .15f);
			break;
		case 1:
            SpawnObject (randomSpaceObject(), transform.position.y, 3f);
			SpawnObject (randomSpaceObject(), Random.Range(20,35), .23f);
			SpawnObject (randomSpaceObject(), Random.Range(5,15), .25f);
			SpawnObject (randomSpaceObject(), Random.Range(-15,0), .26f);
			SpawnObject (randomSpaceObject(), Random.Range(-35,-20), .22f);
			break;
		case 2:
			SpawnObject (randomSetSizedObject (), 35f);
			SpawnObject (randomSpaceObject(), 30, .23f);
			SpawnObject (randomSpaceObject(), 0, .22f);
			SpawnObject (randomSpaceObject(), -30, .23f);
			break;
		case 3:
            SpawnObject (randomSetSizedObject (), -35f);
			SpawnObject (randomSpaceObject(), 30, .25f);
			SpawnObject (randomSpaceObject(), 0, .2f);
			SpawnObject (randomSpaceObject(), -30, .23f);
			break;
		case 4:
            SpawnObject (randomSpaceObject(), Random.Range(-40,40), .15f);
			SpawnObject (randomSpaceObject(), Random.Range(-40,40), .11f);
			SpawnObject (randomSpaceObject(), Random.Range(-40,40), .12f);
			SpawnObject (randomSpaceObject(), Random.Range(-40,40), .13f);
			SpawnObject (randomSpaceObject(), Random.Range(-40,40), .14f);
			SpawnObject (randomSpaceObject(), Random.Range(-40,40), .15f);
			break;
		case 5:
            SpawnObject (randomSpaceObject(), Random.Range(-40,40), .55f);
			SpawnObject (randomSpaceObject(), Random.Range(-40,40), .5f);
			SpawnObject (randomSpaceObject(), Random.Range(-40,40), .16f);
			SpawnObject (randomSpaceObject(), Random.Range(-40,40), .15f);
			SpawnObject (randomSpaceObject(), Random.Range(-40,40), .12f);
			break;
		case 6:
            SpawnObject (randomSpaceObject(), 35f, 4f);
			break;
		case 7:
            SpawnObject (randomSpaceObject(), -20f, 4f);
			SpawnObject (randomSpaceObject(), Random.Range(-40,40), .15f);
			break;
		case 8:
            SpawnObject (randomSpaceObject(), 20f, 4f);
			SpawnObject (randomSpaceObject(), Random.Range(-40,40), .15f);
			SpawnObject (randomSpaceObject(), Random.Range(-40,40), .12f);
			break;
		case 9:
			SpawnObject (randomSpaceObject(), Random.Range(-40,0), .17f);
			SpawnObject (randomSpaceObject(), Random.Range(-40,0), .12f);
			SpawnObject (randomSpaceObject(), Random.Range(-40,0), .19f);
			SpawnObject (randomSpaceObject(), Random.Range(-40,0), .11f);
			SpawnObject (randomSpaceObject(), Random.Range(-40,0), .14f);
			SpawnObject (randomSpaceObject(), Random.Range(-40,0), .14f);
			break;
		case 10:
			SpawnObject (randomSpaceObject(), Random.Range(0,40), .15f);
			SpawnObject (randomSpaceObject(), Random.Range(0,40), .15f);
			SpawnObject (randomSpaceObject(), Random.Range(0,40), .15f);
			SpawnObject (randomSpaceObject(), Random.Range(0,40), .15f);
			SpawnObject (randomSpaceObject(), Random.Range(0,40), .15f);
			SpawnObject (randomSpaceObject(), Random.Range(0,40), .15f);
			break;
		case 11:
            SpawnObject (randomSetSizedObject (), 0f);
			break;
		case 12:
            SpawnObject (randomSetSizedObject (), 0f);
			break;
		case 13:
            SpawnObject (randomSpaceObject(), 5, .06f);
			SpawnObject (randomSpaceObject(), 0, .03f);
			SpawnObject (randomSpaceObject(), -5, .02f);
			SpawnObject (randomSpaceObject(), 1, .05f);
			SpawnObject (randomSpaceObject(), 2, .01f);
			SpawnObject (randomSpaceObject(), -3, .02f);
			break;
		case 14:
            SpawnObject (randomSpaceObject(), transform.position.y, .05f);SpawnObject (randomSpaceObject(), transform.position.y, .05f);SpawnObject (randomSpaceObject(), transform.position.y, .05f);SpawnObject (randomSpaceObject(), transform.position.y, .05f);SpawnObject (randomSpaceObject(), transform.position.y, .05f);SpawnObject (randomSpaceObject(), transform.position.y, .05f);SpawnObject (randomSpaceObject(), transform.position.y, .05f);SpawnObject (randomSpaceObject(), transform.position.y, .05f);SpawnObject (randomSpaceObject(), transform.position.y, .05f);
			break;
		case 15:
            SpawnObject (randomSetSizedObject (), transform.position.y);
			break;
		case 16:
            SpawnObject (randomSpaceObject(), 5, .02f);
			SpawnObject (randomSpaceObject(), 0, .03f);
			SpawnObject (randomSpaceObject(), -5, .04f);
			SpawnObject (randomSpaceObject(), 1, .02f);
			SpawnObject (randomSpaceObject(), 2, .06f);
			SpawnObject (randomSpaceObject(), -3, .07f);
			SpawnObject (randomSpaceObject(), 5, .04f);
			SpawnObject (randomSpaceObject(), 0, .05f);
			SpawnObject (randomSpaceObject(), -5, .04f);
			SpawnObject (randomSpaceObject(), 1, .05f);
			SpawnObject (randomSpaceObject(), 2, .06f);
			SpawnObject (randomSpaceObject(), -3, .05f);
			break;
		case 17:
            SpawnObject (randomSpaceObject(), -3, .05f);SpawnObject (SetSizePrefabs[0], Random.Range(-40,40));SpawnObject (SetSizePrefabs[2], Random.Range(-40,40));SpawnObject (SetSizePrefabs[0], Random.Range(-40,40));SpawnObject (SetSizePrefabs[0], 16f);SpawnObject (SetSizePrefabs[0], Random.Range(-40,40));SpawnObject (SetSizePrefabs[0], Random.Range(-40,40));SpawnObject (SetSizePrefabs[0], Random.Range(-40,40));SpawnObject (SetSizePrefabs[0], Random.Range(-40,40));
			break;
		case 18:
            SpawnObject (randomSetSizedObject (), 35f);
			SpawnObject (randomSetSizedObject (), -35f);
			break;
		case 19:
            SpawnObject (randomSpaceObject(), 35f, 4f);
			SpawnObject (randomSpaceObject(), -35f,4f);
			break;
		case 20:
            SpawnObject (Snek, 20f, 4f);
			break;
		case 21:
			SpawnObject (hole,Random.Range(0,40),2f);
			break;
		default:Debug.Log ("default");
			break;
		}


	
	}


	private GameObject randomSpaceObject(){
		return AsteroidPrefabs[Random.Range(0, AsteroidPrefabs.Length)];
	}

	private GameObject randomSetSizedObject(){
		return SetSizePrefabs[Random.Range(0, SetSizePrefabs.Length)];
	}



	private void SpawnObject(GameObject ob, float y, float scale){
		//y can be between 35 & -35
		var pos = new Vector3(player.transform.position.x+100f,y,0f);
		var asteroid = Instantiate(ob, pos, Quaternion.identity);
		//scale += Random.Range (-.005f,005f);
		asteroid.transform.localScale = new Vector3 (scale,scale,0f);
		asteroid.AddComponent<AstroidCollision> ();

	}

	private void SpawnObject(GameObject ob, float y){
		//y can be between 35 & -35
		var pos = new Vector3(player.transform.position.x+100f,y,0f);
		var asteroid = Instantiate(ob, pos, Quaternion.identity);
		var scale = Random.Range (-.005f,005f);
		asteroid.transform.localScale = new Vector3 (scale,scale,0f);
		asteroid.AddComponent<AstroidCollision> ();
		if (asteroid.GetComponent<Rigidbody2D> () == null) {
			asteroid.AddComponent<Rigidbody2D> ();
			asteroid.GetComponent<Rigidbody2D> ().gravityScale = 0f;
		}

	}

}
