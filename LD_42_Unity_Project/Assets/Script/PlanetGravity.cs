﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlanetGravity : MonoBehaviour
{

    public bool isInRange;

    public bool isStillIn;

    public float isSpeed;

    public float upSpeed;

    public float sideSpeed;

    public float orbSpeed;

    public GameObject player;

    public GameObject colObject;

    public GameObject colObject2;

    //checks last one exit
    public GameObject colObject3;

    //checking for last one entered
    public GameObject colObject4;

    public Vector2 objPosition;

    public Vector2 objPosition2;

    //private Rigidbody2D rd2;

    // Use this for initialization
    void Start()
    {
        isInRange = false;

        isStillIn = false;

        //rd2 = GetComponent<Rigidbody2D>();

    }

    // Update is called once per frame
    void Update()
    {
        if (isInRange == true || isStillIn == true)
        {
            
                //checking if the last item it entered is this one
                if (colObject4 == colObject)
                {

                    player.transform.position = Vector2.MoveTowards(new Vector2(transform.position.x, transform.position.y), objPosition, isSpeed * Time.deltaTime);

                }
                if (colObject4 == colObject2)
                {

                    player.transform.position = Vector2.MoveTowards(new Vector2(transform.position.x, transform.position.y), objPosition2, isSpeed * Time.deltaTime);

                }

        }


    }
    

    private void OnTriggerEnter2D(Collider2D col)
    {
        if (col.gameObject.tag == "Planet")
        {
            isInRange = true;
            colObject4 = col.gameObject;
            
        }

    }

    void OnTriggerStay2D(Collider2D col)
    {
        if (col.gameObject.tag == "Planet")
        {
            isStillIn = true;

            //checkes to see if the two objects are the same name assign
            //if it is not then assigns to the first gameobject
            if (!Object.ReferenceEquals(colObject, colObject2))
            {
                colObject = col.gameObject;

                objPosition = new Vector2(colObject.transform.position.x, colObject.transform.position.y);

               
            }

            //if it is then will assign to the second gameobject
            if (Object.ReferenceEquals(colObject, colObject2))
            {
                colObject2 = col.gameObject;

                objPosition2 = new Vector2(colObject2.transform.position.x, colObject2.transform.position.y);


            }

        }
    }

    void OnTriggerExit2D(Collider2D col)
    {
        //if the two objects are not the same then it will try to exit the one that it was coming out of from
        if (colObject != colObject2)
        {
            colObject3 = col.gameObject;
            if (colObject == colObject3)
            {
                colObject = null;

            }
            if (colObject2 == colObject3)
            {
                colObject2 = null;


            }

        }
        //checks to see if the objects are the same
        //if the same then will turn to false and set to null if exiting
        if (colObject == null && colObject2 == null)
        {

            isStillIn = false;

            isInRange = false;


        }




    }
}
