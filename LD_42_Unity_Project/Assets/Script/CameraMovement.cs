﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraMovement : MonoBehaviour
{    
	//the dev will assign the player 
    public GameObject player;

    //will be used to set the camera position
    private Vector3 offset;

	private Vector2 velocity;
	public float smoothTimeX;
	public float smoothTimeY;

	private float newSmoothTimex;
	private float newSmoothTimey;
	private Vector3 newMinVector;
	private Vector3 newMaxVector;
	public bool bounds=false;
	public Vector3 minCameraPos;
	public Vector3 maxCameraPos;
	public bool cameraMatchsPlayersRotation=false;
	private float startTime;
	private float journeyLength;

    // Use this for initialization
    void Start ()
    {
		startTime = Time.time;
		//sets defualt stats if they get reset to 0
		if (smoothTimeX == 0) {smoothTimeX = .05f;}

		if (smoothTimeY == 0) {smoothTimeY = .05f;

			bounds = false;
			minCameraPos = new Vector3 (-10000000f,40f,-20f);
			maxCameraPos = new Vector3 (999999999999999999999999f,-40f,-20f);
			newMinVector = minCameraPos;
		newMaxVector = maxCameraPos;}
			



		player = GameObject.FindGameObjectWithTag ("Player");
        //is used to start and program so that the player will be followed 
        // camera has to be set in the right spot as the camera if not on player will not follow the player while he is in the game and will not have the player in the middle of the screen
        offset = transform.position - player.transform.position;

		setTarget (player, .5f,.6f, -28f, 28f);
	}

	void Update()
	{
		if (cameraMatchsPlayersRotation) {
			transform.localEulerAngles = player.transform.localEulerAngles;
		}

		if (newSmoothTimex != smoothTimeX) {
			smoothTimeX = Mathf.Lerp (smoothTimeX,newSmoothTimex,300f);

		}
		if (newSmoothTimey != smoothTimeY) {
			smoothTimeY = Mathf.Lerp (smoothTimeY,newSmoothTimey,300f);
		}


	}

	
	// Update is called once per frame
	void FixedUpdate ()
    {
		
			minCameraPos = Vector3.Lerp (minCameraPos, newMinVector, ((Time.time - startTime) * 1f) / 2f);


			maxCameraPos = Vector3.Lerp (maxCameraPos, newMaxVector, ((Time.time - startTime) * 1f) / 2f);




		float posX = Mathf.SmoothDamp (transform.position.x, player.transform.position.x, ref velocity.x, smoothTimeX);
		float posY = Mathf.SmoothDamp (transform.position.y, player.transform.position.y, ref velocity.y, smoothTimeY);

        //the camera will follow whereever the player goes.
		transform.position = new Vector3(posX,posY, transform.position.z);


		
		transform.position = new Vector3 (transform.position.x, Mathf.Clamp (transform.position.y, minCameraPos.y, maxCameraPos.y), transform.position.z);
		
	}


	public void setTarget(GameObject target,float smoothTimex,float smoothTimey,float minY,float maxY){
		player = target;
		newSmoothTimex = smoothTimex;
		newSmoothTimey = smoothTimey;




		newMinVector = new Vector3 (-1000f, minY, -20f);
		newMaxVector = new Vector3 (1000f, maxY, -20f);


		startTime = Time.time;
	}

}

