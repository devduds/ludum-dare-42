﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerChaser : MonoBehaviour {


	private Rigidbody2D rbody;
	private GameObject target;
	private Vector3 targetOldPosition;
	public float time;
	public bool active;
	public float speed;
	// Use this for initialization
	void Start () {
		rbody = GetComponent<Rigidbody2D> ();
		target = GameObject.FindGameObjectWithTag ("Player");
		Physics.IgnoreLayerCollision (8,9);
	}
	
	// Update is called once per frame
	void Update () {
		time += Time.deltaTime;



		if (time > 10f) {
			if (speed < .002f) {
				active = true;
			} else {
				active = false;
			}
		}
	

	}

	void FixedUpdate(){
		speed = ((target.transform.position-targetOldPosition).magnitude)/time;

		targetOldPosition = target.transform.position;


		speed = ((target.transform.position-targetOldPosition).magnitude)/time;

		targetOldPosition = target.transform.position;


		if (active) {
			FollowTargetWithRotation (target.transform, 5f);

		}
	
	}

	/*void FollowTargetWithRotation(Transform target, float distanceToStop, float speed)
	{
		while(Vector3.Distance(transform.position, target.position) > distanceToStop)
		{
			transform.LookAt(target);
			rbody.AddRelativeForce(Vector2.right * speed, ForceMode2D.Force);
		}
	}
*/
	void FollowTargetWithRotation(Transform target, float speed)
	{
		
		transform.right = target.position - transform.position;
		rbody.AddForce(Vector2.right * speed, ForceMode2D.Force);
	}

	void OnCollisionEnter2D(Collision2D col){
		if ((col.gameObject.tag == "Planet")||(col.gameObject.tag == "Asteroid")||(col.gameObject.tag=="Obstical"))
		{

			var force = transform.position - col.transform.position;

			force.Normalize ();
			GetComponent<Rigidbody2D> ().AddForce (-force * 3000);

			if (col.transform.localScale.x > .001f) {
				col.transform.localScale = new Vector3 (col.transform.localScale.x - .1f, col.transform.localScale.y - .1f, 0f);
				transform.localScale=new Vector3 (Mathf.Clamp(transform.localScale.x + .05f,1f,30f), Mathf.Clamp(transform.localScale.y + .05f,1f,30f), 0f);
				if(transform.localScale.x==30f){
					Destroy (this.gameObject);
				}
			}
		}
	}



}
