﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;



public class PlayerMovement : MonoBehaviour
{

    public float speed = 1.5f;

    public float jumpSpeed = 5.0f;
	private Rigidbody2D rbody;


    private Vector2 moveDirection;
   
	void Start(){
		rbody = GetComponent<Rigidbody2D> ();
	}

  
	// Update is called once per frame
	void Update ()
    {
		

        if(Input.GetKey(KeyCode.UpArrow))
        {
            GetComponent<Rigidbody2D>().AddForce(Vector2.up * speed);

        }
        if (Input.GetKey(KeyCode.DownArrow))
        {
            GetComponent<Rigidbody2D>().AddForce(Vector2.down * speed);

        }
        if (Input.GetKey(KeyCode.LeftArrow))
        {
            GetComponent<Rigidbody2D>().AddForce(Vector2.left * speed);

        }
        if (Input.GetKey(KeyCode.RightArrow))
        {
            GetComponent<Rigidbody2D>().AddForce(Vector2.right * speed);

        }
        
    }   


	void OnCollisionEnter2D(Collision2D col){

		if (col.gameObject.name == "GuideBarUp") {

			rbody.AddForce (Vector2.down, ForceMode2D.Impulse);
		}

		if (col.gameObject.name == "GuideBarDown") {

			rbody.AddForce (Vector2.up, ForceMode2D.Impulse);
		}
	}
}

