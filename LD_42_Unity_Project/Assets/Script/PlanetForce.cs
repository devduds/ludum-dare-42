﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlanetForce : MonoBehaviour
{
    public bool isInRange;

    public bool isStillIn;

    public float isSpeed;

  
    public float orbSpeed;

    public bool keyPressed;

    public GameObject player;

    public GameObject colObject;

    public Vector2 objPosition;

   

    // Update is called once per frame
    void Update ()
    {
        if (isInRange == true && keyPressed == false)
        {
            player.transform.position = Vector2.MoveTowards(new Vector2(transform.position.x, transform.position.y), objPosition, isSpeed * Time.deltaTime);

        }
    }

    void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.gameObject.tag == "Player")
        {

        }
    }
}
