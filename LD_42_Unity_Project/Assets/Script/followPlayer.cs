﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class followPlayer : MonoBehaviour {

	private GameObject player;

	// Use this for initialization
	void Start () {
		player = GameObject.FindGameObjectWithTag ("Player");
		transform.position = new Vector2 (player.transform.position.x, player.transform.position.y);
	}
	
	// Update is called once per frame
	void Update () {
		transform.position = new Vector2 (player.transform.position.x, transform.position.y);
	}
}
