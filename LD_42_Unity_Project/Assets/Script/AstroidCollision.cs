﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AstroidCollision : MonoBehaviour {

	private Rigidbody2D rbody;
	private Vector2 dir;
	private float startTime;
	//public float maxSpeed=20f;
	public float maxSpeed=5f;
	private bool topParent=true;
	private float timeAlive;

	// Use this for initialization
	void Start () {
		rbody = GetComponent<Rigidbody2D> ();
		//dir = new Vector2 (Random.Range (-1, 1), Random.Range (-1, 1));
		dir=Vector2.left;

		rbody.AddForce(dir);
		float startTime = Time.deltaTime;
	}







	// Update is called once per frame
	void Update () {
		timeAlive += Time.deltaTime;
		if (timeAlive > 60f) {
			Destroy (this.gameObject);
		}


	}

	void FixedUpdate(){
		if (rbody.velocity.magnitude > maxSpeed) {
			rbody.velocity=rbody.velocity.normalized*maxSpeed;
		}
		if (( Time.deltaTime-startTime) < 5f&&(topParent)) {
			rbody.AddForce(dir);
		}
	}

	void OnTriggerEnter2D(Collider2D col){
		if (col.gameObject.name == "DestroyZone") {

			Destroy (this.gameObject);
		}
	}

	void OnCollisionEnter2D(Collision2D col){
		if (col.gameObject.name == "DestroyZone") {
			
			Destroy (this.gameObject);
		}

		if (col.gameObject.name == "GuideBarUp") {

			rbody.AddForce (Vector2.down, ForceMode2D.Impulse);
		}

		if (col.gameObject.name == "GuideBarDown") {

			rbody.AddForce(Vector2.up,ForceMode2D.Impulse);
		}
	


		if (col.transform.localScale.x > transform.localScale.x) {
			if (col.gameObject.tag == "Planet" || col.gameObject.tag == "Asteroid") {
				transform.SetParent (col.transform);
				topParent = false;
			}
		}

	}

	void OnCollisionExit2D(Collision2D col){
		transform.SetParent (null);
		topParent = true;
	}


}
