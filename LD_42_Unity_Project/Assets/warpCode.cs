﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class warpCode : MonoBehaviour {

	public float gravStrength=100f;


	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {



	}


	void OnTriggerStay2D(Collider2D col){
		if ((col.gameObject.name != "GuideBarUp") &&(col.gameObject.name!="GuideBarDown")&&(col.gameObject.name!="Grav"))
		{

			if (col.GetComponent<Rigidbody2D> () != null) {
			Vector3 forceDirection = transform.position - col.transform.position;

				col.GetComponent<Rigidbody2D> ().AddForce (forceDirection.normalized * Time.fixedDeltaTime * gravStrength);
			}

			col.transform.Rotate (new Vector3 (0, 0, 45f * Time.deltaTime));
		}

	}






}
