﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlackHole : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void OnTriggerStay2D(Collider2D col){
		Debug.Log (col.gameObject.name);

		if (col.gameObject.tag != "Player") {
			col.transform.localScale = new Vector3 (col.transform.localScale.x - .01f, col.transform.localScale.y - .01f, 0f);
			if (col.transform.localScale.x < .02f) {
				Destroy (col.gameObject);
			}
		}
	}

	void OnTriggerExit2D(Collider2D col){
		
	}
}
