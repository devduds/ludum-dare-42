﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Eat : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void OnCollisionEnter2D(Collision2D col){
		if ((col.gameObject.tag == "Planet")||(col.gameObject.tag == "Asteroid")||(col.gameObject.tag=="Obstical"))
		{

			var force = transform.position - col.transform.position;

			force.Normalize ();
			GetComponent<Rigidbody2D> ().AddForce (-force * 3000);

			if (col.transform.localScale.x > .001f) {
				col.transform.localScale = new Vector3 (col.transform.localScale.x - .1f, col.transform.localScale.y - .1f, 0f);
				transform.localScale=new Vector3 (Mathf.Clamp(transform.localScale.x + .05f,1f,30f), Mathf.Clamp(transform.localScale.y + .05f,1f,30f), 0f);
			}
		}
	}
}
